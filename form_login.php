<div class="wrap-form-contact"> 
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-contact">
					<h3>Form Login</h3>
					
					<form>
						<div class="form-group">
							<input type="email" class="form-control" id="email" placeholder="Email">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="pwd" placeholder="Password">
						</div>
						<div class="checkbox">
							<label><input type="checkbox"> Remember me</label>
							<a href="#">Forgot Password?</a>
						</div>
						<button type="submit" class="btn btn-default">login</button>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>